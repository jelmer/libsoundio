Source: libsoundio
Maintainer: Debian QA Group <packages@qa.debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper (>= 9),
               cmake,
               doxygen,
               libasound2-dev,
               libjack-jackd2-dev,
               libpulse-dev,
               pkg-config
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/multimedia-team/libsoundio
Vcs-Git: https://salsa.debian.org/multimedia-team/libsoundio.git
Homepage: http://libsound.io/

Package: libsoundio-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: libsoundio1 (= ${binary:Version}),
         ${misc:Depends}
Description: cross platform audio input and output library (development files)
 libsoundio is a lightweight abstraction over various sound drivers. It provides
 a well-documented API that operates consistently regardless of the sound driver
 it connects to. It performs no buffering or processing on your behalf; instead
 exposing the raw power of the underlying backend.
 .
 libsoundio is appropriate for games, music players, digital audio workstations,
 and various utilities.
 .
 libsoundio is serious about robustness. It even handles out of memory
 conditions correctly.
 .
 This package contains the development files.

Package: libsoundio1
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends},
         ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: cross-platform audio input and output library
 libsoundio is a lightweight abstraction over various sound drivers. It provides
 a well-documented API that operates consistently regardless of the sound driver
 it connects to. It performs no buffering or processing on your behalf; instead
 exposing the raw power of the underlying backend.
 .
 libsoundio is appropriate for games, music players, digital audio workstations,
 and various utilities.
 .
 libsoundio is serious about robustness. It even handles out of memory
 conditions correctly.
 .
 This package contains the shared library.

Package: libsoundio-dbg
Architecture: any
Multi-Arch: same
Section: debug
Depends: libsoundio1 (= ${binary:Version}),
         ${misc:Depends}
Description: debugging symbols for libsoundio
 This package contains the debugging symbols for libsoundio1.
